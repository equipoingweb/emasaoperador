/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import cliente.avisoCliente;
import java.io.Serializable;
import java.util.ArrayList;

import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.faces.bean.ManagedBean;

import javax.faces.event.AjaxBehaviorEvent;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.Response;
import model.Aviso;

@ManagedBean(name = "avisoBean", eager = true)
@SessionScoped
public class AvisoBean implements Serializable {

    // private List<Operacion> misOperaciones;
    // private List<Adjunto> misAdjuntos;
    private List<Aviso> listaAvisos;
    private Aviso avisoSeleccionado;
    private String latitud, longitud, tipo, descripcion, urgencia, estado, email, rutaAdjunto, inputEmail, inputPalabras, urlMaps, id;
    
    /**
     * Creates a new instance of AvisoBean
     */
    public AvisoBean() {
        latitud = "";
        longitud = "";
        tipo = "";
        descripcion = "";
        urgencia = "-1";
        estado = "En cola";
        email = "";
        rutaAdjunto = "";
        urlMaps = "";
        inputEmail = "";
        inputPalabras = "";
        id = "";
        
    }

    @PostConstruct
    public void cargarSeleccionado() {
        obtenerAvisos();
    }

    public void cargarDatadoAviso(Aviso a){
          if (a == null) {
            latitud = "";
            longitud = "";
            tipo = "";
            descripcion = "";
            urgencia = "-1";
            estado = "En cola";
            email = "";
            rutaAdjunto = "";
            urlMaps = "";
            id = "";
        } else {
            latitud = String.valueOf(a.getLatitud());
            longitud = String.valueOf(a.getLongitud());
            tipo = a.getTipo();
            descripcion = a.getDescripcion();
            urgencia =String.valueOf( a.getUrgencia());
            estado = a.getEstado();
            email = a.getEmailUsuario();
            urlMaps = a.getUrlMaps();
            id = String.valueOf(a.getIdAviso());
        }
    }
    public String verAviso(Aviso a) {
        cargarDatadoAviso(a);
        return "aviso.xhtml";
    }
    
    public String editarAviso(String id){
        cargarDatadoAviso(avisoSeleccionado);
        return "formularioCrearAviso.xhtml";
    }

    public void obtenerAvisos() {

        avisoCliente c = new avisoCliente();
        Response response = c.findAll_JSON(Response.class);

        GenericType<List<Aviso>> genericType = new GenericType<List<Aviso>>() {
        };
// Returns an ArrayList of Players from the web service
        List<Aviso> data = new ArrayList<Aviso>();
        data = response.readEntity(genericType);
        setListaAvisos(data);

    }

    public Aviso obtenerAviso(){
          avisoCliente c = new avisoCliente();
        Response response = c.find_JSON(Response.class, id);

        GenericType<Aviso> genericType = new GenericType<Aviso>() {
        };
// Returns an ArrayList of Players from the web service
        Aviso data = new Aviso();
        data = response.readEntity(genericType);
        setAvisoSeleccionado(avisoSeleccionado);
        return avisoSeleccionado;
    }
    public void resetearCampos() {
        inputEmail = "";
        inputPalabras = "";
    }

    public void recargarTodos(AjaxBehaviorEvent event) {
        resetearCampos();
        obtenerAvisos();
    }
   
    public void recargarPorEmail(AjaxBehaviorEvent event) {
        inputPalabras = "";
        if (inputEmail != null && !inputEmail.equals("")) {
            avisoCliente c = new avisoCliente();
            Response response = c.findEmail_JSON(Response.class, inputEmail);

            GenericType<List<Aviso>> genericType = new GenericType<List<Aviso>>() {
            };
            // Returns an ArrayList of Players from the web service
            List<Aviso> data = new ArrayList<Aviso>();
            data = response.readEntity(genericType);
            setListaAvisos(data);
        }

    }

    public void recargarPorPalabrasClave(AjaxBehaviorEvent event) {
        inputEmail = "";
        if (inputPalabras != null && !inputPalabras.equals("")) {
            avisoCliente c = new avisoCliente();
            Response response = c.findPalabrasClave_JSON(Response.class, inputPalabras);

            GenericType<List<Aviso>> genericType = new GenericType<List<Aviso>>() {
            };
            // Returns an ArrayList of Players from the web service
            List<Aviso> data = new ArrayList<Aviso>();
            data = response.readEntity(genericType);
            setListaAvisos(data);
        }

    }

    public void recargarAbiertos(AjaxBehaviorEvent event) {
        resetearCampos();

        if (inputEmail != null && !inputEmail.equals("")) {
            avisoCliente c = new avisoCliente();
            Response response = c.findAbiertos_JSON(Response.class);

            GenericType<List<Aviso>> genericType = new GenericType<List<Aviso>>() {
            };
            // Returns an ArrayList of Players from the web service
            List<Aviso> data = new ArrayList<Aviso>();
            data = response.readEntity(genericType);
            setListaAvisos(data);
        }
    }

    public List<Aviso> getListaAvisos() {
        return listaAvisos;
    }

    public void setListaAvisos(List<Aviso> listaAvisos) {
        this.listaAvisos = listaAvisos;
    }

    public Aviso getAvisoSeleccionado() {
        return avisoSeleccionado;
    }

    public void setAvisoSeleccionado(Aviso avisoSeleccionado) {
        this.avisoSeleccionado = avisoSeleccionado;
    }

    public String getInputEmail() {
        return inputEmail;
    }

    public void setInputEmail(String inputEmail) {
        this.inputEmail = inputEmail;
    }

    public String getInputPalabras() {
        return inputPalabras;
    }

    public void setInputPalabras(String inputPalabras) {
        this.inputPalabras = inputPalabras;
    }

    public String getLatitud() {
        return latitud;
    }

    public void setLatitud(String latitud) {
        this.latitud = latitud;
    }

    public String getLongitud() {
        return longitud;
    }

    public void setLongitud(String longitud) {
        this.longitud = longitud;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getUrgencia() {
        return urgencia;
    }

    public void setUrgencia(String urgencia) {
        this.urgencia = urgencia;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUrlMaps() {
        return urlMaps;
    }

    public void setUrlMaps(String urlMaps) {
        this.urlMaps = urlMaps;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    
}
